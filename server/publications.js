//publications

Meteor.publish('proposte', function () {
    return Proposte.find();
});

Meteor.publish('voti', function () {
    return Voti.find();
});

Meteor.publish('archivio', function () {
    return Archivio.find();
});

Meteor.publish('votiArchivio', function () {
    return VotiArchivio.find();
});

Meteor.publish('archivioProposte', function () {
    return ArchivioProposte.find();
});

Meteor.publish('fuoriConcorso', function () {
    return FuoriConcorso.find();
});

Meteor.publish('cicli', function () {
    return Cicli.find();
});

Meteor.publish('silvanoBot', function () {
    return SilvanoBot.find();
});

