//aggiungo gli utenti e gli admin su Houston
Houston.add_collection(Meteor.users);
Houston.add_collection(Houston._admins);

Meteor.methods({

    submitCommento: function (_id, commento) {
        if (!Meteor.userId()) {
            throw new Meteor.Error("not-authorized");
        };

        if (commento === "") {
            Proposte.update({
                _id: _id
            }, {
                $unset: {
                    commento: ""
                }
            });
        } else {
            //aggiungere sanitizeHtml
            Proposte.update({
                _id: _id
            }, {
                $set: {
                    commento: commento
                }
            });
        }
    },

    cambiaData: function (data) {
        if (!Meteor.userId()) {
            throw new Meteor.Error("not-authorized");
        }

        Proposte.update({
            utente: Meteor.user().username
        }, {
            $set: {
                data: data
            }
        });
    },

    togglePartecipo: function (destinatario, partecipo) {
        if (!Meteor.userId()) {
            throw new Meteor.Error("not-authorized");
        }

        Voti.update({
            destinatario: destinatario,
            mittente: Meteor.user().username
        }, {
            $set: {
                partecipo: partecipo
            }
        }, {
            upsert: true
        });
    },


    aggiungiProposta: function (film) {
        if (!Meteor.userId()) {
            throw new Meteor.Error("not-authorized");
        }

        Proposte.update({
            utente: Meteor.user().username
        }, {
            $addToSet: {
                films: film
            }
        }, {
            upsert: true //se non esiste lo aggiunge
        });

        //è cambiata la corrispNumId quindi cancello i voti relativi
        Meteor.call("azzeraVoti", Meteor.user().username);
    },

    eliminaFilm: function (imdbID) {
        if (!Meteor.userId()) {
            throw new Meteor.Error("not-authorized");
        }

        Proposte.update({
            utente: Meteor.user().username
        }, {
            $pull: {
                films: {
                    imdbID: imdbID
                }
            }
        });

        var numFilms = Proposte.findOne({
            utente: Meteor.user().username
        }).films.length;
        //se ho eliminato tutti i film
        if (numFilms === 0) {
            //elimino anche il record vuoto
            console.log("rimuovo perché vuoto:", Meteor.user().username)
            Proposte.remove({
                utente: Meteor.user().username
            });
        }

        //è cambiata la corrispNumId quindi cancello i voti relativi
        Meteor.call("azzeraVoti", Meteor.user().username);

    },
    submitVoto: function (nomeDestinatario, vettoreVoto, corrispNumId) {
        if (!Meteor.userId()) {
            throw new Meteor.Error("not-authorized");
        }

        Voti.update({
            mittente: Meteor.user().username,
            destinatario: nomeDestinatario
        }, {
            $set: {
                vettore: vettoreVoto,
                corrispNumId: corrispNumId
            }
        }, {
            upsert: true //se non esiste lo aggiunge
        });
    },

    azzeraVoti: function (nomeDestinatario) {
        if (!Meteor.userId()) {
            throw new Meteor.Error("not-authorized");
        }

        Voti.remove({
            destinatario: nomeDestinatario
        })
        console.log("cancello voti ", nomeDestinatario)

    },

    aggiungiCiclo: function (tema) {
        Meteor.call("controllaUtenteAdmin")
        Cicli.insert({
            data: new Date(),
            tema: tema,
            chiuso: false
        })
    },

    toggleCiclo: function (_id) {
        Meteor.call("controllaUtenteAdmin")
        var ciclo = Cicli.findOne({
            _id: _id
        })
        if (ciclo.chiuso) {
            Cicli.update({
                _id: _id
            }, {
                $unset: {
                    chiuso: ""
                }
            })
        } else {
            //trova i film appartenenti al ciclo
            var archivioCiclo = Archivio.find({
                ciclo: _id
            }).fetch()

            archivioCiclo.forEach(function (x) {
                Archivio.update({
                    _id: x._id
                }, {
                    $set: {
                        media: calcolaMedia(x._id)
                    }
                })
            })


            Cicli.update({
                _id: _id
            }, {
                $set: {
                    chiuso: "true"
                }
            })
        }
    },

    rimuoviCiclo: function (_id) {
        Meteor.call("controllaUtenteAdmin")
        Cicli.remove({
            _id: _id
        })
    },

    controllaUtenteAdmin: function () {
        var loggedInUser = Meteor.user()
            //controllo se sono admin
        if (!loggedInUser ||
            !Roles.userIsInRole(loggedInUser, 'admin')) {
            throw new Meteor.Error(403, "Access denied")
        }
    },

    aggiungiFilmArchivio: function (imdbID) {
        Meteor.call("controllaUtenteAdmin")

        //console.log("voglio aggiungere", imdbID)
        addendo = Proposte.findOne({
            "films.imdbID": imdbID
        }, {
            fields: {
                utente: 1,
                "films.$": 1 //altrimenti mi restituisce tutto l'array films INTERO!
            }
        })

        //console.log(addendo);


        //prendo l'ultimo ciclo per data (quindi quello corrente)
        var ciclo = Cicli.findOne({}, {
            sort: {
                data: -1
            }
        })

        console.log("ciclo", ciclo)

        Archivio.insert({
            utente: addendo.utente,
            ciclo: ciclo._id,
            film: addendo.films[0], //è un array con un solo elemento -_-
        });

    },

    extendArchivio: function (_id, dati) {
        Meteor.call("controllaUtenteAdmin")
        console.log(_id, dati)

        var film = Archivio.findOne({
            _id: _id
        }).film

        _.extend(film, dati)

        Archivio.update({
            _id: _id
        }, {
            $set: {
                film: film
            }
        })
    },

    votaArchivio: function (voto, _id) {
        if (!Meteor.userId()) {
            throw new Meteor.Error("not-authorized");
        }

        VotiArchivio.update({
            mittente: Meteor.user().username,
            destinatario: _id
        }, {
            $set: {
                voto: voto
            }
        }, {
            upsert: true
        })
    },

    archiviaProposta: function (proposta) {

        Meteor.call("controllaUtenteAdmin")

        var votiProposta = Voti.find({
            destinatario: proposta.utente
        }, {
            fields: {
                _id: 0
            }
        }).fetch()

        //tolgo l'_id sennò non è unico
        propostaSenza_id = _.omit(proposta, "_id")

        //aggiungo i voti alla proposta
        //come un array
        propostaSenza_id.voti = votiProposta


        //lo aggiungo (con lo stesso _id della proposta per sempl.)
        ArchivioProposte.insert(propostaSenza_id);

        //elimino la proposta!
        Proposte.remove({
            _id: proposta._id
        });

        //cancello anche i voti per quella proposta!
        Meteor.call("azzeraVoti", proposta.utente);

    },

    aggiungiFuoriConcorso: function (film) {
        Meteor.call("controllaUtenteAdmin")

        FuoriConcorso.insert(film)

    },

    rimuoviFuoriConcorso: function (_id) {
        Meteor.call("controllaUtenteAdmin")

        FuoriConcorso.remove(_id)

    },

    //sORt è "s" per la ricerca e "t" per un film solo
    searchOMDb: function (sORt, query) {
        this.unblock(); //asincrona
        console.log("cerco in OMDb", query)
        return Meteor.http.get("http://www.omdbapi.com/?" + sORt + "=" + query + "&type=movie");
    },

    
    searchSGMEDIAIMDB: function(title) {
        this.unblock(); //asincrona
        var query       = title.replace(/ /g, "_")
        var first_char  = title[0].toLowerCase()
        // var url         = "http://sg.media-imdb.com/suggests/" + first_char + '/' + query + '.json'
        var url         = "https://v2.sg.media-imdb.com/suggests/" + first_char + '/' + query + '.json'

        var response    =  Meteor.http.get(url).content
        var response2   = response.match(/\(.*\)/)[0]
        var length      = response2.length
        var response3   = response2.substring(1, length - 1 )
        var json        = JSON.parse(response3)
        var films       = json.d
        var finalResults = []
        films.forEach(function (x, idx) {
            if(x.q == 'feature'){
                var image = undefined
                if(x.i){
                    var image = x.i[0]
                }
                finalResults.push({
                    "imdbID" : x.id,
                    "Poster" : image,
                    "Year" : x.y,
                    "Title" : x.l,
                    "Actors": x.s,
                })
            }
        }
        )
        return finalResults
    },

    // searchTMDb: function(query) {
    //     var response = Meteor.http.get("https://api.themoviedb.org/3/search/company?api_key=11b17888b57ec9194239bc2f55092da4&query=" + encodeURI(query))
    //     var finalResults = []

    // },

    findTMDb: function (x) {
        var url = "https://api.themoviedb.org/3/find/" + x.imdbID + "?api_key=11b17888b57ec9194239bc2f55092da4&language=en-US&external_source=imdb_id"
        var response = Meteor.http.get(url)
        var json = JSON.parse(response.content)
        var movie = json.movie_results[0]
        if(new Date(movie.release_date) < new Date()){
            var url = ""
            var poster_path = "http://image.tmdb.org/t/p/w45/" + movie.poster_path

            var finalResults = {
                "Title" : movie.original_title,
                "Year" : new Date(movie.release_date).getFullYear(),
                "imdbID" : x.imdbID,
                "Type" : "movie",
                "Released" : movie.relase_date,
                "Plot" : movie.overview,
                "imdbRating" : movie.vote_average,
                "imdbVotes" : movie.vote_count,
                "Response" : "True",
                "background_image" : poster_path,
                "small_cover_image" : poster_path,
                "small_cover_image" : poster_path,
            }

            var response = Meteor.http.get("https://api.themoviedb.org/3/movie/" + movie.id + "?api_key=11b17888b57ec9194239bc2f55092da4&language=en-US")
            var json     = JSON.parse(response.content)
            finalResults.Runtime = json.runtime + 'min'
            finalResults.Genre   = json.genres.map(function(obj) { return obj.name}).join(", ")
            finalResults.Country  = json.production_countries.map(function(obj) { return obj.name}).join(", ")
            finalResults.language = json.spoken_languages.map(function(obj) { return obj.name}).join(", ")
            return finalResults
        } else {
            return undefined
        }
    },

    searchYify: function (query, limit) {
        this.unblock(); //asincrona
        console.log("cerco in yify", query);
        if (!limit) {
            limit = 1
        }
        return Meteor.http.get("https://yts.ag/api/v2/list_movies.json?lang=it&limit=" + limit + "&query_term=" + query)
    },

    //non serve più da quando non c'è l'api specifica per popcorntime
    /*    searchYifyId: function (id) {
            this.unblock(); //asincrona
            console.log("cerco in yify per id", id);
            return Meteor.http.get("https://yts.ag/api/v2/movie_details.json?with_images=true&movie_id=" + id)
        },*/

    searchYifySubtitles: function (query) {
        this.unblock //asincrona
        console.log("cerco in YifySubs", query)
        return Meteor.http.get("http://api.yifysubtitles.com/subs/" + query)
    }
});



/* ESEMPIO STRUTTURA DATI
proposte: [
            {
                utente: "giulio",
                films: [{
                        film: "a"
                        },
                    {
                        film: "b"
                        },
                    {
                        film: "c"
                        }
                    ]
            },
            {
                utente: "pippo",
                films: [{
                        film: "a"
                        },
                    {
                        film: "b"
                        },
                    {
                        film: "c"
                        }
                    ]
            },
            {
                utente: "sasasa",
                films: [{
                        film: "a"
                        },
                    {
                        film: "b"
                        },
                    {
                        film: "c"
                        }
                    ]
            }
            ]*/