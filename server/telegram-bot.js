//per telegram bot

Meteor.startup(function () {

    var leleCounter = 0 //quante volte lele si accolla

    var token = process.env.TELEGRAM_TOKEN || "170263095:AAFKH5CIt_qDwc4gD7rEfDeNmJEugfCag_Q"

    console.log("token " + token)
    TELEGRAM_URL = "https://api.telegram.org/bot" + token + "/"

    console.log("ROOT_URL", process.env.ROOT_URL)

    if (process.env.ROOT_URL == "http://localhost:3000/") {
        //sono in locale
        //uso il polling e disattivo il webhook
        TelegramPolling = true
        Meteor.http.post(TELEGRAM_URL + "setWebhook")
            //imposto anche l'offset per il polling
        TelegramUpdatesOffset = 0

        //faccio partire il polling
        var polling = function () {
            if (TelegramPolling) {
                var updates = Meteor.http.get(TELEGRAM_URL + "getUpdates", {
                    params: {
                        offset: TelegramUpdatesOffset + 1
                    }
                })
                updates.data.result.map(function (item) {
                    console.log(item)
                    TelegramUpdatesOffset = item.update_id;
                    processaMessaggio(item)
                })
            }
            Meteor.setTimeout(polling, 1000)
        }
        Meteor.setTimeout(polling, 1000)
    } else {
            //sono su heroku o chichessia
            //imposto il webhook
            TelegramPolling = false
            Meteor.http.post(TELEGRAM_URL + "setWebhook", {
                params: {
                    url: process.env.ROOT_URL + "/hook" || "https://peaceful-beyond-9767.herokuapp.com/hook"
                }
            })
    }

    /*
    *bold text*
_italic text_
[text](URL)
    */

    //array di oggetti {command:"sdfs",callback:function(msg,from,item.message)}
    TelegramTriggers = [{
        command: "/salutami",
        callback: function (msg, from) {
            //31325107 è Lele
            //18341527 sono io
            if (31325107 == from.id) {
                leleCounter += 1
                if (leleCounter >= 2) {
                    return "Lele, te lo dico per la " + leleCounter + "° volta, " + choice(["accanna", "smettila", "basta così", "hai rotto", "finiscila"]) + "."
                } else {
                    return "Basta, Lele."
                }
            } else {
                var risposta = "Ciao " + from.first_name + "!\n"
                risposta += rispondi(5)
                return risposta
            }
        }
    }, {
        command: "/proposte",
        callback: function (msg, from) {
            return "Ciao " + from.first_name + ", le [proposte](" + process.env.ROOT_URL + ") sono:\n\n" + proposte()
        }
    }, {
        command: "/risultati",
        callback: function (msg, from) {
            return "Ciao " + from.first_name + ", la [classifica](" + process.env.ROOT_URL + ") attuale è:\n\n" + risultati()
        }
    }, {
        command: "/parla",
        callback: function (msg, from) {
            return rispondi(msg.length, msg)
        }
    }]
})

var risultati = function () {

    var risposta = ""

    //destinatari unici dei voti
    var destinat = _.uniq(Voti.find({
        partecipo: true,
        vettore: {
            $exists: true
        }
    }).map(function (x) {
        return x.destinatario;
    }));

    var classifiche = []
    destinat.forEach(function (dest) {
        classifiche.push({
            classifica: calcolaClassifica(calcolaStrongestPaths(calcolaMatriceSomma(dest)), dest),
            utente: dest
        })
    })
    classifiche.forEach(function (a) {
        risposta += "*" + a.utente + "*:\n"
        a.classifica.forEach(function (x) {
            risposta += x.punteggio + " - [" + x.film.Title + "](" + "http://www.imdb.com/title/" + x.film.imdbID + ") _(" + x.film.Year + ")_\n"
        })
        risposta += "\n(con i voti di: "
        var voti = _.map(Voti.find({
            destinatario: a.utente,
            partecipo: true
        }).fetch(), function (voto) {
            return "_" + voto.mittente + "_"
        })
        risposta += voti.join(", ") + ")\n"
    })

    risposta += "\n[Clicca qui per aprire il sito](" + process.env.ROOT_URL + ")\n"

    return risposta
}

var proposte = function () {
    var risposta = ""
    var proposte = Proposte.find().fetch()
    proposte.forEach(function (proposta) {
        risposta += "*" + proposta.utente + "*:\n"
        if (proposta.commento) {
            var commentoPurificato = sanitizeHtml(proposta.commento, {
                allowedTags: ['b', 'i', 'a'],
                allowedAttributes: {
                    'a': ['href']
                }
            })
            risposta += "_" + commentoPurificato + "_\n"
        }
        proposta.films.forEach(function (film) {
            risposta += "  - [" + film.Title + "](" + "http://www.imdb.com/title/" + film.imdbID + ") _(" + film.Year + ")_\n"
        })
    })
    return risposta
}

/*
SilvanoBot è un array di object
{
    key : ["parola","parola2]
    wordstats : ["sdfvs","dsfsdf"]
}
*/

var choice = function (a) {
    var i = Math.floor(a.length * Math.random());
    return a[i];
};

var rispondi = function (min_length, seed) {
    var risposta = []
    var next = {}

    //cerco nei seed se ho qualche key
    if (seed) {
        for (var i = 1; i < seed.length; i++) {
            console.log("seed[" + i + "]=" + seed[i])
            next = SilvanoBot.findOne({
                key: seed[i]
            })
            if (!_.isEmpty(next)) {
                //metto la parola iniziale
                risposta.push(seed[i])
                break;
            }
        }
    }

    //se non ho trovato nessuna key
    //prendo una parola tra le iniziali
    if (_.isEmpty(next)) {
        var iniziali = SilvanoBot.find({
                iniziale: true
            }).fetch()
            //console.log(_.pluck(iniziali, "key"))
        var parolaIniziale = choice(_.pluck(iniziali, "key"))
        console.log(parolaIniziale)
        if (parolaIniziale) {
            //metto le parole iniziali flatten
            risposta.push(parolaIniziale)
            next = SilvanoBot.findOne({
                key: parolaIniziale
            })
        }
    }

    while (!_.isEmpty(next) && (risposta.length < min_length * 50)) {
        console.log("next " + next.key, next.wordstats)
        risposta.push(choice(next.wordstats))
            //console.log("risposta finora " + risposta)
        var next = SilvanoBot.findOne({
            key: risposta[risposta.length - 1]
        })
    }
    return risposta.join(" ") || "non so bene che rispondere..."
};

//markov chain ordine 1
var salvaMarkov = function (msg) {
    if (msg.length > 3) {
        //salvo la parola iniziale
        console.log("salvo iniziale " + msg[0])
        SilvanoBot.update({
            key: msg[0]
        }, {
            $push: {
                wordstats: msg[1]
            },
            $set: {
                iniziale: true
            }
        }, {
            upsert: true
        })

        //aggiungo fino alla fine
        for (var j = 1; j < msg.length - 1; j++) {
            SilvanoBot.update({
                key: msg[j]
            }, {
                $push: {
                    wordstats: msg[j + 1]
                }
            }, {
                upsert: true
            })
        }
    }
}


processaMessaggio = function (item) {
    if (!_.has(item, "message")) {
        if (_.has(item, "edited_message")) {
            item.message = item.edited_message
        }
    }

    console.log("ricevuto messaggio " + item.message.text)

    var chatId = item.message.chat.id;
    var from = item.message.from;

    if (msg = item.message.text) {
        // splits string into an array 
        // and removes the @botname from the command
        // then returns the array
        msg = msg.split(" ")
        msg[0] = msg[0].split('@')[0]
        console.log(msg[0])
        //cerco se ho un trigger uguale a msg[0]
        var obj = _.find(TelegramTriggers, function (obj) {
            return obj.command == msg[0].toLowerCase()
        })

        if (obj) {
            //ho un trigger quindi rispondo
            var risposta = obj.callback(msg, from, item.message)
            //send
            Meteor.call("sendTelegramMessage", risposta, chatId)
        } else {
            //flag per disattivare la memorizzazione dei msg della chat
            //che spesso impalla il sito
            var TelegramMarkovAttivo = process.env.TELEGRAM_MARKOV_ATTIVO
            if(TelegramMarkovAttivo){
                salvaMarkov(msg)
            }
        }
    }
}


// da terminale 
// curl -H "Content-Type: application/json" -d '{"message":"foo"}' http://localhost:3000/hook

Router.route('hook', {
        where: 'server'
    })
    .post(function () {

        var item = this.request.body

        processaMessaggio(item)

        // Watch the Meteor log to see this output
        console.log("Hook called.");
        //console.log("Headers: ", this.request.headers);
        console.log("Data: ", this.request.body);

        this.response.writeHead(200, {
            'Content-Type': 'text/html'
        });
        this.response.write("You wrote: " + this.request.body.message);
        this.response.write("\n");

        // `this.response.end` *must* be called, or else the connection is left open.
        this.response.end('Success!\n');
    })



Meteor.methods({
    sendTelegramMessage: function (text, chatId, mostraRisultati) {
        if (mostraRisultati) {
            text = text + risultati()
        }
        console.log("invio messaggio: '" + text + "' a '" + chatId + "'")
        Meteor.http.post(TELEGRAM_URL + "sendMessage", {
            params: {
                chat_id: chatId || process.env.DEFAULT_CHAT_ID || 18341527,
                text: text,
                parse_mode: "Markdown"
            }
        })
    }
})