Template.fuoriConcorso.helpers({
    films: function () {
        return FuoriConcorso.find()
    }
})

Template.fuoriConcorso.events({
    "click .rimuoviFuoriConcorso": function (event, template) {
        Meteor.call("rimuoviFuoriConcorso", this._id)
    }
})