Session.setDefault("ordinaPerDelta", false)

Template.classificaUtenti.helpers({
  users: function () {
    var users = Meteor.users.find({username: {$ne: 'Miaogiulia'}})
    users = users.map(function (user) {
      // var films = Archivio.find({ utente: user.username, media: {$ne: null} }).fetch()
      var films = Archivio.find({ utente: user.username }).fetch()
      films = films.map( function (film) {
        if ((film.media == NaN) || (film.media == null) || !film.media) {
          var votes = VotiArchivio.find({destinatario: film._id}).fetch()
          if (votes.length > 0) {
            var numbers = votes.map(function (v) {
              return v.voto
            })
            var somma = numbers.reduce(function (a,b) {
              return a + b
            }) 
            media = somma / votes.length 
            media = parseFloat(media.toFixed(2))
            film.media = media
          }
          return film
        }
        else
        {
          return film
        }
      })
      films = films.filter( function (film) {
        return film.media
      })
      
      user.films = films.sort( function (a,b) {
        cicloA = Cicli.findOne({_id: a.ciclo})
        cicloB = Cicli.findOne({_id: b.ciclo})
        if (cicloA && cicloB) {
          dataA = cicloA.data
          dataB = cicloB.data
          if (dataA < dataB)
            return -1;
          else if (dataA > dataB)
            return 1;
          else 
            return 0;
        } else {
          return 0;
        }
        }
      )
     
      if (user.films.length > 0) {
        votiFilm = user.films.map(function(film) {
          return film.media
        })
        user.mediaTotale = (votiFilm.reduce(function(a,b) {
          return a + b
        }) / user.films.length).toFixed(2)
      } else {
        user.mediaTotale = 0
      }
      return user
    })

    users = users.filter(function(user) { 
      return user.films.length > 0
    })
    return users.sort( function(a,b) {
        if (a.mediaTotale < b.mediaTotale)
          return 1;
        else if (a.mediaTotale > b.mediaTotale)
          return -1;
        else 
          return 0;
      }
    )
  },

  ordinaPerDelta: function () {
    return Session.get("ordinaPerDelta")
  },

  movies: function () {
    // var movies = Archivio.find({media: {$ne: null}})
    var movies = Archivio.find().fetch()
    movies = movies.map(function (movie) {
      var votes = VotiArchivio.find({ destinatario: movie._id}).fetch()
      movie.votes = votes
      if ((movie.media == NaN) || (movie.media == null) || !movie.media) {
        if (movie.votes.length > 0) {
          var numbers = movie.votes.map(function (v) {
            return v.voto
          })
          var somma = numbers.reduce(function (a,b) {
            return a + b
          }) 
          media = somma / votes.length 
          media = parseFloat(media.toFixed(2))
          movie.media = media
        }
      }
      movie.votes = votes.sort( function(a,b) {
        if (a.voto < b.voto)
            return 1;
          else if (a.voto > b.voto)
            return -1;
          else 
            return 0;
      })
      movie.delta = +(movie.media - (movie.film.imdbRating / 2)).toFixed(3)
      return movie
    })
    movies = movies.filter( function (movie) {
      return movie.votes.length>0
    })
    if (Session.get("ordinaPerDelta")) {
      return movies.sort(function (a, b) {
          if (a.delta < b.delta)
              return 1;
          else if (a.delta > b.delta)
              return -1;
          else
              return 0;
      })
    } else {
      return movies.sort( function(a,b) {
        if (a.media < b.media)
          return 1;
        else if (a.media > b.media)
          return -1;
        else 
          return 0;
      })
    }
  },

  incremented: function(index) {
    index++;
    return index;
  },

  half: function(number) {
    return (number / 2).toFixed(2)
  },

  graficoUserFilms: function() {
    var medie = this.films.map(function (film) {
      return film.media
    })
    var series = [{
      name: 'FILM',
      data: medie,
      fillColor : {
                    linearGradient : {
                        x1: 0,
                        y1: 0,
                        x2: 0,
                        y2: 1
                    },
                    stops : [
                        [0, Highcharts.getOptions().colors[0]],
                        [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                    ]
                },
    }]
    var categories = this.films.map(function (film) {
      return film.film.Title
    })
    chart = {
        colors: ["#DDDF0D", "#7798BF", "#55BF3B", "#DF5353", "#aaeeee", "#ff0066", "#eeaaee",
    "#55BF3B", "#DF5353", "#7798BF", "#aaeeee"],
        chart: {
            type: 'area',
            backgroundColor: {
              linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
              stops: [
                [0, 'rgb(96, 96, 96)'],
                [1, 'rgb(12, 12, 12)']
              ]
            },
            borderWidth: 0,
            borderRadius: 0,
            plotBackgroundColor: null,
            plotShadow: false,
            plotBorderWidth: 0
        },
        title: {
            text: "SILVANI OTTENUTI DA " + this.username,
            style: {
              fontSize: '17px',
              fontWeight: 'bold'
            }
        },
        xAxis: {
            gridLineWidth: 0,
            lineColor: '#999',
            tickColor: '#999',
            labels: {
              style: {
                color: '#FFFFFF',
                fontWeight: 'bold',
                fontSize: '13px'
              }
            },
            categories: categories
        },
        yAxis: {
            alternateGridColor: null,
            minorTickInterval: null,
            gridLineColor: 'rgba(255, 255, 255, .1)',
            minorGridLineColor: 'rgba(255,255,255,0.07)',
            lineWidth: 0,
            tickWidth: 0,
            labels: {
              style: {
                color: '#FFFFFF',
                fontWeight: 'bold',
                fontSize: '13px'
              }
            },
            title: {
                text: 'SILVANI',
                style: {
                  color: '#FFFFFF',
                  fontWeight: 'bold',
                  fontSize: '17px'
                }
            },
            min: 0,
            max: 5,
            tickInterval: 1,
        },
        plotOptions: {
          series: {
            shadow: true
          },
          candlestick: {
            lineColor: '#404048'
          },
          map: {
            shadow: false
          }
        },
        legend: {
          itemStyle: {
            fontWeight: 'bold',
            fontSize: '15px',
            color: "#FFFFFF"
          }
        },
        tooltip: {
          formatter: function () {
                    return '<b>' + this.x + '</b><br/>' +
                        'Silvani: ' + this.y + '<br/>';
                }
        },
        series: series
    };

    return chart;
  }


})

Template.classificaUtenti.events({
    "change .Delta": function (event, template) {
        Session.set("ordinaPerDelta", template.find("#fancy-checkbox-defaultDelta").checked)
        console.log(Session.get("ordinaPerDelta"))
    },
})
