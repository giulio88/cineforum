Template.Home.onCreated(function () {
    Meteor.subscribe('proposte');
    Meteor.subscribe('voti');
    Meteor.subscribe('archivio');
    Meteor.subscribe('votiArchivio');
    //Meteor.subscribe('archivioProposte'); //spostata al caricamento della pagina
    Meteor.subscribe('fuoriConcorso');
    Meteor.subscribe('cicli');
});

