Template.navbar.helpers({
    attivo: function (template) {
        var currentRoute = Router.current();

        //lookupTemplate() restituisce con la maiuscola
        //il mio template invece è minuscolo
        //non capisco perché, ma nel dubbio uso toUpperCase()
        return currentRoute &&
            template.toUpperCase() === currentRoute.lookupTemplate().toUpperCase() ?
            'active' : '';
    }
})