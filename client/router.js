/*        {{> aggiungi }}
        
        
        {{> proposte}}
                

        {{> risultati}}

        
        {{> voti}}
        
        
        {{> archivio}}*/

Router.configure({
    layoutTemplate: 'Home'
});

Router.route('/', function () {
    this.render('proposte')
}, {
    name: 'Proposte'
});

Router.route('proposte');
Router.route('risultati');

Router.route('archivio');

Router.route('app-dump');

Router.route('archivioProposte');

Router.route('fuoriConcorso');

Router.route('statistiche');

Router.route('classificaUtenti');

Router.route('silvanoBot');
