Session.setDefault("risultatiRicerca", false);
Session.setDefault('searching', {
    searching: false,
    caricamento: 5
});

var lista = []

Template.aggiungi.events({

    "submit .cerca-film": function (event, template) {

        event.preventDefault();

        Session.set("risultatiRicerca", [])
        Session.set('searching', {
            searching: true,
            caricamento: 5 //tanto per dire che ho cominciato
        });

        var query = template.$('input[type=text]').val();


        //"s" restituisce un array di 10 film
        //"t" restituisce più info su un film solo
        //"i" come "t" ma con l'id imdb
        //prendo l'array dei 10 film usando s= come parametro
        Meteor.call("searchSGMEDIAIMDB", query,
            function (err, res) { //callback
                if (err) {
                    console.log(err)

                    //FIX QUANDO NON FUNZIONA OMDB
                    //cerco solo su yify allora (anche se troverò molti meno risultati)
                    Meteor.call("searchYify", query, 50,
                        function (err, res) {
                            console.log("Non funzionava omdb quindi ho cercato solo su yify")
                            if (err) {
                                console.log(err)
                            } else {
                                var datiYify = JSON.parse(res.content)

                                console.log("datiYify", datiYify)

                                //uso slice() perché sennò memorizza per riferimento
                                listaMigliore = datiYify.data.movies.slice()

                                listaMigliore.forEach(function (x, idx) {
                                    //yify lo salva in quest'altro punto.. =S
                                    x.imdbID = x.imdb_code

                                    //orrendamente copiaincollata... che schifo...
                                    Meteor.call("searchYifySubtitles", x.imdbID,
                                        function (err, res) {
                                            if (err) {
                                                console.log(err)
                                                    //Session.set("risultatiRicerca", false)
                                            } else {
                                                var datiYifySubtitles = JSON.parse(res.content)
                                                console.log("datiYifySubtitles", datiYifySubtitles)
                                                if (datiYifySubtitles.subtitles) {
                                                    //prendo solo ita e eng
                                                    var subs = _.pick(datiYifySubtitles.subs[x.imdbID], "italian", "english")
                                                    _.extend(listaMigliore[idx], subs)
                                                        //console.log("listaMigliore yify", listaMigliore)
                                                    Session.set("risultatiRicerca", listaMigliore)
                                                }
                                            }
                                        })
                                })

                                Session.set("risultatiRicerca", listaMigliore)

                                //dovrei aver finito di caricare, tolgo la progressbar
                                Session.set('searching', {
                                    searching: false,
                                    caricamento: 5
                                });

                            }
                        })


                } else {

                    //lista è globale
                    // lista = parse.Search # omdb api
                    lista = res

                    var listaMigliore = []

                    //poiché voglio più dettagli faccio varie ricerche per ogni film
                    //e aggiungo (_.extend) nella listaMigliore i dati OMDb e Yify
                    //mano a mano che arrivano
                    var ctr = 0
                    lista.forEach(function (x, idx) {

                        Meteor.call("findTMDb", x,
                            function (err, res) {
                                ctr ++

                                if (err) {
                                    console.log(err)
                                } else {
                                    var datiFilm = res
                                    // _.extend(listaMigliore[idx], datiFilm)
                                    if(res){
                                        listaMigliore.push(datiFilm)

                                        Session.set("risultatiRicerca", listaMigliore)

                                        //quanti film della lista hanno i dati completi (sfrutto il campo regista)
                                        var completi = _.compact(_.pluck(Session.get("risultatiRicerca"), "imdbRating")).length
                                        var caricamento = completi / lista.length * 100
                                            
                                    }  
                                }

                                if (ctr == lista.length){
                                    Session.set('searching', {
                                        searching: false,
                                        caricamento: 100
                                    });

                                } else { 
                                    Session.set('searching', {
                                        searching: true,
                                        caricamento: caricamento
                                    });
                                }
                            })

                        // Meteor.call("searchYify", x.imdbID,
                        //     function (err, res) {
                        //         if (err) {
                        //             console.log(err)
                        //                 //Session.set("risultatiRicerca", false)
                        //         } else {
                        //             var datiYify = JSON.parse(res.content)

                        //             console.log("datiYify", datiYify)

                        //             if (datiYify.data.movies[0]) {
                        //                 _.defaults(listaMigliore[idx], datiYify.data.movies[0])
                        //                 Session.set("risultatiRicerca", listaMigliore)
                        //             }

                        //         }
                        //     })


                    })

                    
                }

            });

    
    }
})

Template.aggiungi.helpers({
    risultatiRicerca: function () {
        return Session.get("risultatiRicerca");
    },
    searching: function () {
        return Session.get('searching').searching;
    },
    caricamento: function () {
        //console.log("caricamento", Session.get("searching").caricamento)
        return Session.get("searching").caricamento
    }
})

Template.aggiungi.events({
    "click .submit-film": function (event, template) {
        event.preventDefault();
        console.log("voglio aggiungere", this)
        Meteor.call("aggiungiProposta", this)
        Session.set("risultatiRicerca", false);
    },
    "click .submit-fuoriConcorso": function (event, template) {
        event.preventDefault();
        console.log("voglio aggiungere ai fuori concorso", this)
        Meteor.call("aggiungiFuoriConcorso", this)
        Session.set("risultatiRicerca", false);
    }
})

