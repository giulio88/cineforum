Template.gestisciCicli.events({

    "submit .aggiungiCiclo": function (event, template) {

        event.preventDefault();

        var tema = template.$('input[type=text]').val();

        console.log("tema", tema)

        Meteor.call("aggiungiCiclo", tema)
    },
    "click .rimuoviCiclo": function (event, template) {
        Meteor.call("rimuoviCiclo", this._id)
    },
    "click .toggleCiclo": function (event, template) {
        Meteor.call("toggleCiclo", this._id)
    },
})

Template.gestisciCicli.helpers({
    cicli: function () {
        return Cicli.find({}, {
            sort: {
                data: -1
            }
        })
    }
})