Template.mappa.onRendered(function () {

    var self = this

    //lo rendo reattivo ai bottoni
    self.autorun(function () {

        var paesi = _.pluck(
            _.compact(
                _.pluck(Archivio.find().fetch(), "film")
            ), "Country")

        console.log("paesi", paesi)
        var paesiArchivio = []
        ArchivioProposte.find().fetch().forEach(function (pr) {
            pr.films.forEach(function (film) {
                paesiArchivio.push(film.Country)
            })
        })
        console.log("paesiArchivio", paesiArchivio)

        //controllo quali dati mostrare
        var paesiDaIncludere = []
        if (Session.get("mostraOscar")) {
            console.log("mostraoscar")
            paesiDaIncludere = paesiDaIncludere.concat(paesi)
        }
        if (Session.get("mostraArchivio")) {
            console.log("mostraarchivio")
            paesiDaIncludere = paesiDaIncludere.concat(paesiArchivio)
        }
        console.log("paesiDaIncludere", paesiDaIncludere)

        var paesiSdoppiati = paesiDaIncludere.map(function (x) {
            return x.split(", ")
        })
        console.log("paesiSdoppiati", paesiSdoppiati)

        //prendo tutti i paesi
        var paesiFlatten = _.flatten(paesiSdoppiati)
        console.log("paesiFlatten", paesiFlatten)

        var paesiConto = _.map(_.pairs(_.countBy(paesiFlatten)),
            function (x) {
                //correggo incongruenze con la mappa
                var paese = x[0]
                if (paese === "USA") {
                    paese = "United States of America"
                } else if (paese === "UK") {
                    paese = "United Kingdom"
                }
                return {
                    "name": paese,
                    "value": x[1]
                }
            })

        console.log("paesiConto", paesiConto)
        console.log("mappa", Highcharts.maps['custom/world-eckert3'])

        Template.instance().$('#mappaFilm').highcharts('Map', {

            title: {
                text: 'Distribuzione geografica'
            },

            mapNavigation: {
                enabled: true
            },

            colorAxis: {
                type: 'exponential',
                minColor: '#484fff',
                maxColor: '#b70000'
            },

            series: [{
                name: "Numero film",
                data: paesiConto,
                animation: {
                    duration: 1000
                },
                mapData: Highcharts.maps['custom/world-eckert3'],
                joinBy: ['name', 'name'],
                tooltip: {
                    pointFormat: '{point.name}: {point.value}'
                },
                //showCheckbox: true
                }]
        });
    })
})

Session.setDefault("mostraOscar", true)
Session.setDefault("mostraArchivio", true)

Template.statistiche.events({
    "change .Oscar": function (event, template) {
        Session.set("mostraOscar", template.find("#fancy-checkbox-defaultOscar").checked)
        console.log(Session.get("mostraOscar"))
    },
    "change .Archivio": function (event, template) {
        Session.set("mostraArchivio", template.find("#fancy-checkbox-defaultArchivio").checked)
        console.log(Session.get("mostraArchivio"))
    },
})


Template.statistiche.helpers({
    mostraOscar: function () {
        return Session.get("mostraOscar")
    },
    mostraArchivio: function () {
        return Session.get("mostraArchivio")
    },
    graficoCombinatoAnni: function () {

        var utenti = _.uniq(_.compact(_.pluck(ArchivioProposte.find({}, {
            fields: {
                utente: 1
            }
        }).fetch(), "utente")))
        console.log("utenti archiviati", utenti)


        // creo un secondo set di colori trasp
        var coloriTrasp = Highcharts.map(Highcharts.getOptions().colors, function (color) {
            //console.log(color, Highcharts.Color(color))
            return Highcharts.Color(color)
                .brighten(-0.3)
                //.setOpacity(0.5)
                .get('rgba');
        });

        //associo gli utenti a un colore della palette
        //sennò in ogni serie hanno colori diversi
        var coloreUtente = _.object(utenti, Highcharts.getOptions().colors)
        var coloreUtenteTrasp = _.object(utenti, coloriTrasp)
        console.log(coloreUtente)


        var anniUtenti1 = utenti.map(function (u) {
            var an = _.map(_.pairs(_.countBy(_.pluck(
                _.compact(
                    _.pluck(Archivio.find({
                        utente: u
                    }).fetch(), "film")
                ), "Year"), function (a) {
                return Number(a)
            })), function (arr) {
                return [Number(arr[0]), arr[1]]
            })
            return {
                name: u.concat(" (Oscar)"),
                data: an,
                stack: "oscar",
                color: coloreUtente[u]
            }
        })

        console.log("anniUtenti1", anniUtenti1)


        var anniUtenti = utenti.map(function (u) {
            var propUt = _.pluck(ArchivioProposte.find({
                    utente: u
                }).fetch(),
                "films")
            var anniUt = []
            propUt.forEach(function (pr) {
                pr.forEach(function (film) {
                    anniUt.push(film.Year)
                })
            })
            var an = _.map(_.pairs(_.countBy(anniUt, function (a) {
                return Number(a)
            })), function (arr) {
                return [Number(arr[0]), arr[1]]
            })
            return {
                name: u.concat(" (Archivio)"),
                data: an,
                stack: "archiviata",
                color: coloreUtenteTrasp[u]
            }
        })

        //controllo quali dati mostrare
        var series = []
        if (Session.get("mostraOscar")) {
            series = series.concat(anniUtenti1)
        }
        if (Session.get("mostraArchivio")) {
            series = series.concat(anniUtenti)
        }


        chart =  {
            chart: {
                type: 'column',
                zoomType: 'x'
            },
            title: {
                text: 'Distribuzione Temporale'
            },
            xAxis: {
                title: {
                    text: 'Anno'
                },
                categories: [] //non ha molto senso ma altrimenti non mette
                    //tutti gli indici degli anni
            },
            yAxis: {
                title: {
                    text: 'Film'
                },
                tickInterval: 1,
                stackLabels: {
                    enabled: true,
                }
            },

            tooltip: {
                formatter: function () {
                    return '<b>' + this.x + '</b><br/>' +
                        this.series.name + ': ' + this.y + '<br/>' +
                        'Totale: ' + this.point.stackTotal;
                }
            },
            plotOptions: {
                series: {
                    stacking: 'normal'
                }
            },
            series: series
        };
        console.log(chart)
        return chart
    },
})