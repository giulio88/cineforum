Template.registerHelper('dataItaliana', function (data) {
    moment.locale('it');
    return moment(data).format('LL');
});