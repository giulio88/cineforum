Template.aggiornaData.events({
    "blur .cambia-data": function (event) {
        moment.locale('it');
        event.preventDefault();
        console.log(moment(event.target.value, 'LL').toDate())
        Meteor.call("cambiaData", moment(event.target.value, 'LL').toDate());
    }
})

Template.aggiornaData.helpers({
    oggi: function () {
        return moment().format('LL')
    }
})

Template.aggiornaData.onRendered(function () {
    this.$('#datetimepicker').datetimepicker({
        format: 'LL',
        locale: 'it'
    });
});