Template.archivioProposte.onCreated(function () {
    Meteor.subscribe('archivioProposte');
});


Template.archivioProposte.helpers({
    proposte: function () {
        return ArchivioProposte.find({}, {
            sort: {
                data: -1
            }
        })
    }
})

Template.propostaArchivio.helpers({

    //quasi un duplicato di quello in risultati.js
    corrispondenze: function (voto) {

        /*        console.log("calcolo corrisp", voto)
                console.log("parentData", Template.parentData())*/

        if (voto.partecipo) {

            var vettore = voto.vettore;
            var corrispNumId = voto.corrispNumId;

            filmVoto = []
            Template.parentData().films.forEach(function (movie) {
                filmVoto.push({
                    film: movie,
                    voto: vettore[corrispNumId[movie.imdbID]]
                })
            });

            filmVoto.sort(function (a, b) {
                return a.voto - b.voto;
            });

            return filmVoto
        }
    }
})