Session.setDefault("nuovoUtente", 1)


Template.proposte.helpers({

    proposte: function () {
        return Proposte.find({}, {
            sort: {
                data: 1
            }
        })
    },
    nuovoUtente: function () {
        return Session.get("nuovoUtente");
    },
    temaCiclo: function () {
        return Cicli.findOne({}, {
            sort: {
                data: -1
            }
        }).tema
    }
});

Template.proposte.onRendered(function () {
    this.autorun(function () {
        if (Meteor.user()) {
            var prop = Proposte.find({
                utente: Meteor.user().username
            }).fetch();
            //console.log(prop)
            if (!prop.length) {
                console.log("questo utente non ha un record", Meteor.user().username)
                Session.set("nuovoUtente", 1)
            } else {
                console.log("questo utente ha un record", Meteor.user().username)
                Session.set("nuovoUtente", 0)
            }
        }
    })
})


Template.editCommento.onRendered(function () {
    Template.instance().$('textarea').autosize();
})

Template.proposta.events({

    "change .partecipo": function (event, template) {
        var partecipo = template.find("input").checked
        var destinatario = this.utente
        Meteor.call("togglePartecipo", destinatario, partecipo)
    },

    "click .elimina": function (event, template) {
        console.log(this, this.imdbID)
        Meteor.call("eliminaFilm", this.imdbID);
    },

    "click .submit-voto": function (event, template) {

        //prendo l'elenco dei film della tale persona
        proposteTizio = Proposte.findOne({
            //this._id è l'id delle proposte del tizio sul quale sto votando
            _id: this._id
        });
        var nomeDestinatario = proposteTizio.utente
        var totFilms = proposteTizio.films.length;

        //memorizzo un object literal di corrispondenze tra id film e il suo indice
        //sicuramente c'è un metodo piu diretto e semplice
        //ma non ho voglia di pensarci ora
        var corrispNumId = {}
        var vettoreVoto = [];

        proposteTizio.films.forEach(function (movie, key) {
            corrispNumId[movie.imdbID] = key;
            //gia' che ci sono metto di default 
            //tutti i film all'ultimo posto
            vettoreVoto[key] = totFilms
        });

        console.log("corrispNumId", corrispNumId);
        console.log("vettoreVotoPRIMA", vettoreVoto);

        //becco tutti i radiobutton della mia istanza
        var inputVoto = Template.instance().findAll('input[type=radio]');
        console.log(inputVoto)

        //memorizzo la classifica in vettoreVoto
        inputVoto.forEach(function (questo) {
            //questo.value è il voto (posizione) che do al film
            //questo.id è l'id imdb del film alla tale posizione
            //ATTENZIONE!! NON È questo.imdbID perché l'ho salvato
            //nell'html come id={{this.imdbID}}
            if (questo.checked) {
                console.log(questo)
                console.log(questo.value);
                console.log(questo.id);
                console.log(questo.checked);
                vettoreVoto[corrispNumId[questo.id]] = parseInt(questo.value);
            }
        });
        console.log("vettoreVotoDOPO", vettoreVoto);

        Meteor.call("submitVoto", nomeDestinatario, vettoreVoto, corrispNumId);
        Meteor.call("sendTelegramMessage", Meteor.user().username + " ha votato, i risultati aggiornati sono:\n\n", undefined, true)
    },

    "click .submit-archivio": function (event, template) {
        console.log("archivio film visto ", this, "e proposta", template.data)

        Meteor.call("aggiungiFilmArchivio", this.imdbID)

        //occhio: questo la cancella!
        Meteor.call("archiviaProposta", template.data)
    }

});

Template.proposta.helpers({

    dataItaliana: function (data) {
        moment.locale('it');
        return moment(data).format('LL');
    },

    isPartecipo: function (utente) {
        var questoVoto = Voti.findOne({
            destinatario: utente,
            mittente: Meteor.user().username
        })
        return questoVoto && questoVoto.partecipo
    },

    altroUtente: function (utente) {
        return Meteor.userId() ? utente !== Meteor.user().username : true;
    },
    arrayNumeroFilms: function (utente) {
        //console.log("arraynumfilms", utente)
        questaProposta = Proposte.findOne({
            utente: utente
        });

        var foo = []; //creo un array da 0 a length

        for (var i = 1; i <= questaProposta.films.length; i++) {
            foo.push(i);
        }
        return foo;
    },
    isChecked: function (destinatario, filmId, value) {
        var questoVoto = Voti.findOne({
            mittente: Meteor.user().username,
            destinatario: destinatario,
        });
        //console.log("questoVoto", questoVoto)
        if (questoVoto && questoVoto.vettore) {
            var corrispNumId = questoVoto.corrispNumId;
            var vettoreVoto = questoVoto.vettore;
            //console.log(destinatario, vettoreVoto[corrispNumId[filmId]], value,
            //  vettoreVoto[corrispNumId[filmId]] === value)
            return vettoreVoto[corrispNumId[filmId]] === value;
        } else {
            return false
        }
    }
});

Template.editCommento.events({
    "click .submit-commento": function (event, template) {
        event.preventDefault()
        var markdownCode = template.$('textarea').val()
            //console.log(markdownCode)
        Meteor.call("submitCommento", this._id, markdownCode)
    }
})

