Template.risultati.helpers({
    matrici: function () {
        var risposta = []

        //destinatari unici dei voti
        var destinat = _.uniq(Voti.find({
            partecipo: true,
            vettore: {
                $exists: true
            }
        }).map(function (x) {
            return x.destinatario;
        }));

        var contatorePerId = 0
        destinat.forEach(function (dest) {
            sum = calcolaMatriceSomma(dest);
            strpath = calcolaStrongestPaths(sum);
            risposta.push({
                _id: contatorePerId,
                destinatario: dest,
                matriceSomma: sum,
                matriceStrongestPaths: strpath,
                classifica: calcolaClassifica(strpath, dest)
            })
            contatorePerId = contatorePerId + 1
        })
        console.log(risposta)
        return risposta;
    }

})

Template.tabellaMatrice.helpers({
    titoli: function (utente) {
        return Proposte.findOne({
            utente: utente
        }).films
    }
})

Template.voti.helpers({
    votiDest: function (dest) {
        var voti = Voti.find({
            destinatario: dest,
            partecipo: true
        }).fetch();
        console.log(voti)
        return voti;
    },

    corrispondenze: function (_id) {

        var voto = Voti.findOne({
            _id: _id
        })

        var dest = voto.destinatario;
        var vettore = voto.vettore;
        var corrispNumId = voto.corrispNumId;

        //console.log("************corrispDest", corrispDest, dest)

        filmVoto = []
        Proposte.findOne({
            utente: dest
        }).films.forEach(function (movie) {
            filmVoto.push({
                film: movie,
                voto: vettore[corrispNumId[movie.imdbID]]
            })
        });

        filmVoto.sort(function (a, b) {
            return a.voto - b.voto;
        });

        return filmVoto
    }
});