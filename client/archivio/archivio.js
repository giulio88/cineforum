Template.archivio.helpers({
    cicli: function () {
        //console.log(_.pluck(Archivio.find({}).fetch(), "ciclo"))
        /*        
                //ho dei cicli vecchi che non corrispondono sul db
                //li estraggo qui
                cicli = _.uniq(_.pluck(Archivio.find({}, {
                    sort: {
                        ciclo: -1 //ultimi cicli in cima
                    }
                }).fetch(), "ciclo"))*/

        //nuovi cicli
        var cicli = Cicli.find({}, {
            sort: {
                data: -1
            }
        })

        return cicli
    },

    aperto: function (IdCiclo) {
        return (!Cicli.findOne({
            _id: IdCiclo
        }).chiuso)
    },

    archivio: function (IdCiclo) {
        console.log("IdCiclo", IdCiclo)
        return Archivio.find({
            ciclo: IdCiclo
        })
    },

    //_id è l'id del destinatario del voto in VotiArchivio
    //cioè è l'id dell'elemento in Archivio destinatario del voto
    mediaVoti: function (_id) {
        if (Meteor.user()) {
            return calcolaMedia(_id) || "[vota per conoscere]"
        }
    },
    votanti: function (_id) {

        //mi restituisce una lista di Object
        var votiObject = VotiArchivio.find({
            destinatario: _id
        }).fetch()

        return _.pluck(votiObject, "mittente")
    },
    voto: function (_id, num) {
        var questoVoto = VotiArchivio.findOne({
            destinatario: _id,
            mittente: Meteor.user().username
        }, {
            fields: {
                voto: 1
            }
        })
        voto = questoVoto ? questoVoto.voto : 0;
        //console.log("voto", voto)
        //console.log("mittente", Meteor.user().username)
        //console.log("dest", _id)

        return voto >= num ? "voto" : "";
    },
    vincitore: function (IdCiclo) {

        var filmCiclo = Archivio.find({
            ciclo: IdCiclo
        }).fetch()
        var vincitore = filmCiclo[0]
        filmCiclo.forEach(function (x) {
            if (x.media > vincitore.media) {
                vincitore = x
                console.log("sorpassato da " + vincitore.film.Title)
            }
        })
        return vincitore
    }
})

Template.archivio.events({

    "click svg": function (event, template) {
        //event.currentTarget.id è il voto numerico
        //this._id è quale record ho votato in Archivio
        //console.log("event.currentTarget.id", event.currentTarget.id)
        //console.log(this._id)
        Meteor.call("votaArchivio", parseFloat(event.currentTarget.id), this._id)
    },
    "click .riscaricaDati": function (event, template) {
        console.log("riscarico i dati per ", this._id)
        var _id = this._id
        var imdbID = this.film.imdbID

        console.log("riscarico per ", _id, imdbID)

        //ORRENDAMENTE COPIATE DA aggiungi.js
        //tocca fare una funzione generale!!!

        Meteor.call("findTMDb", this.film,
            function (err, res) {
                if (err) {
                    console.log(err)
                        //Session.set("risultatiRicerca", false)
                } else {
                    var datiFilm = res

                    console.log("aggiungo", datiFilm)
                    Meteor.call("extendArchivio", _id, datiFilm)

                }
            })

        Meteor.call("searchYify", imdbID,
            function (err, res) {
                if (err) {
                    console.log(err)
                        //Session.set("risultatiRicerca", false)
                } else {
                    var datiYify = JSON.parse(res.content)
                    if (datiYify.data.movies[0]) {
                        console.log("aggiungo", datiYify.data.movies[0])
                        Meteor.call("extendArchivio", _id, datiYify.data.movies[0])
                    }
                }

            })

        Meteor.call("searchYifySubtitles", imdbID,
            function (err, res) {
                if (err) {
                    console.log(err)
                        //Session.set("risultatiRicerca", false)
                } else {
                    var datiYifySubtitles = JSON.parse(res.content)
                    if (datiYifySubtitles.subtitles) {
                        //prendo solo ita e eng
                        var subs = _.pick(datiYifySubtitles.subs[imdbID], "italian", "english")

                        console.log("aggiungo", subs)
                        Meteor.call("extendArchivio", _id, subs)
                    }
                }

            })

    }
})