Session.setDefault("fraseCreata", null);

Template.silvanoBot.onCreated(function () {
    //commentato perché è una ciofeca e impalla tutto
    //Meteor.subscribe('silvanoBot');
});


Template.silvanoBot.events({
    "click #inviaMessaggio": function (event, template) {
        //solo se sono utente posso mandare
        if (Meteor.userId()) {
            var text = Session.get("fraseCreata")
            Meteor.call("sendTelegramMessage", text)
        }
    }
})

Template.silvanoBot.helpers({
    fraseCreata: function () {
        return Session.get("fraseCreata")
    }
})



Template.silvanoBot.onRendered(function () {

    var self = this

    var width = $('#tree').width()
    var height = $(window).height()

    var i = 0,
        duration = 750,
        root;

    var tree = d3.layout.tree()
        .size([height, width]);

    self.autorun(function () {

        var silvano = SilvanoBot.find().fetch()


        //prendo le parole iniziali
        var iniziali = _.pluck(SilvanoBot.find({
            iniziale: true
        }).fetch(), "key")

        //le trasformo in oggetti col campo nome
        var inizialiObject = iniziali.map(function (x) {
            return {
                name: x
            }
        })

        //mi serve che parta da 1 elemento solo!
        //gli metto come children le parole iniziali
        var silvanoBase = {
            name: "inizio",
            children: inizialiObject
        }


        var diagonal = d3.svg.diagonal()
            .projection(function (d) {
                return [d.y, d.x];
            });



        // Function to center node when clicked/dropped so node doesn't get lost when collapsing/moving with large amount of children.

        function centerNode(source) {
            scale = zoomListener.scale();
            x = -source.y0;
            y = -source.x0;
            x = x * scale + width / 2;
            y = y * scale + height / 2;
            group.transition()
                .duration(duration)
                .attr("transform", "translate(" + x + "," + y + ")scale(" + scale + ")");
            zoomListener.scale(scale);
            zoomListener.translate([x, y]);
        }



        // Define the zoom function for the zoomable tree

        function zoom() {
            group.attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
        }


        // define the zoomListener which calls the zoom function on the "zoom" event constrained within the scaleExtents
        var zoomListener = d3.behavior.zoom().scaleExtent([0.1, 3]).on("zoom", zoom);




        var svg = d3.select("#tree")
            .attr("width", width)
            .attr("height", height)
            .attr("class", "overlay")
            .call(zoomListener)

        //ripulisco dalle schifezze
        svg.selectAll("*").remove();

        var group = svg.append("g")
            //            .attr("transform", "translate(" + margin.left + "," + margin.top + ")")


        var link = group.selectAll(".link")
        var node = group.selectAll(".node")


        root = silvanoBase;
        root.x0 = height / 2;
        root.y0 = 0;


        function aggiungiChildren(d) {
            //cerco i children nel db
            console.log("cerco i children di", d)
            var parola = SilvanoBot.findOne({
                key: d.name
            })
            if (parola) {
                console.log("ho trovato queste parole", parola.wordstats)
                var children = []
                parola.wordstats.forEach(function (x) {
                    //creo l'oggetto che ancora non esiste
                    var oggetto = {
                            name: x
                        }
                        //lo pusho in children
                    children.push(oggetto)
                })

                d.children = children

            }
        }

        function collapse(d) {
            if (d.children) {
                d._children = d.children;
                d._children.forEach(collapse);
                d.children = null;
            }
        }

        console.log("root", root)
        root.children.forEach(aggiungiChildren)
        root.children.forEach(collapse);
        update(root);
        centerNode(root)


        function update(source) {

            // Compute the new height, function counts total children of root node and sets tree height accordingly.
            // This prevents the layout looking squashed when new nodes are made visible or looking sparse when nodes are removed
            // This makes the layout more consistent.
            var levelWidth = [1];
            var childCount = function (level, n) {

                if (n.children && n.children.length > 0) {
                    if (levelWidth.length <= level + 1) levelWidth.push(0);

                    levelWidth[level + 1] += n.children.length;
                    n.children.forEach(function (d) {
                        childCount(level + 1, d);
                    });
                }
            };
            childCount(0, root);
            var newHeight = d3.max(levelWidth) * 25; // 25 pixels per line  
            tree = tree.size([newHeight, width]);

            // Compute the new tree layout.
            var nodes = tree.nodes(root).reverse(),
                links = tree.links(nodes);

            // Normalize for fixed-depth.
            nodes.forEach(function (d) {
                d.y = d.depth * 120;
            });

            // Update the nodes…
            var node = group.selectAll("g.node")
                .data(nodes, function (d) {
                    return d.id || (d.id = ++i);
                });

            // Enter any new nodes at the parent's previous position.
            var nodeEnter = node.enter().append("g")
                .attr("class", "node")
                .attr("transform", function (d) {
                    return "translate(" + source.y0 + "," + source.x0 + ")";
                })
                .on("click", click);

            nodeEnter.append("circle")
                .attr("r", 1e-6)
                .style("fill", function (d) {
                    return d._children ? "#a51010" : "#69e092";
                });

            nodeEnter.append("text")
                .attr("x", function (d) {
                    return d.children || d._children ? -10 : 10;
                })
                .attr("dy", ".35em")
                .attr("text-anchor", function (d) {
                    return d.children || d._children ? "end" : "start";
                })
                .text(function (d) {
                    return d.name;
                })
                .style("fill-opacity", 1e-6);

            // Transition nodes to their new position.
            var nodeUpdate = node.transition()
                .duration(duration)
                .attr("transform", function (d) {
                    return "translate(" + d.y + "," + d.x + ")";
                });

            nodeUpdate.select("circle")
                .attr("r", 4.5)
                .style("fill", function (d) {
                    return d._children ? "#d31a1a" : "#22d12f";
                });

            nodeUpdate.select("text")
                .style("fill", "white")
                .style("fill-opacity", 1);

            // Transition exiting nodes to the parent's new position.
            var nodeExit = node.exit().transition()
                .duration(duration)
                .attr("transform", function (d) {
                    return "translate(" + source.y + "," + source.x + ")";
                })
                .remove();

            nodeExit.select("circle")
                .attr("r", 1e-6);

            nodeExit.select("text")
                .style("fill-opacity", 1e-6);

            // Update the links…
            var link = group.selectAll("path.link")
                .data(links, function (d) {
                    return d.target.id;
                });

            // Enter any new links at the parent's previous position.
            link.enter().insert("path", "g")
                .attr("class", "link")
                .attr("d", function (d) {
                    var o = {
                        x: source.x0,
                        y: source.y0
                    };
                    return diagonal({
                        source: o,
                        target: o
                    });
                });

            // Transition links to their new position.
            link.transition()
                .duration(duration)
                .attr("d", diagonal);

            // Transition exiting nodes to the parent's new position.
            link.exit().transition()
                .duration(duration)
                .attr("d", function (d) {
                    var o = {
                        x: source.x,
                        y: source.y
                    };
                    return diagonal({
                        source: o,
                        target: o
                    });
                })
                .remove();

            // Stash the old positions for transition.
            nodes.forEach(function (d) {
                d.x0 = d.x;
                d.y0 = d.y;
            });
        }

        //funz ricorsiva per calcolare la frase creata
        function calcolaFrase(source, frase) {
            if (!frase) {
                var frase = []
            }
            if ("parent" in source) {
                //console.log("sono in ", source, "e la frase è ", frase)
                frase.unshift(source.name)
                calcolaFrase(source.parent, frase)
            }
            return frase.join(" ")
        }

        // Toggle children on click.
        function click(d) {
            console.log("cliccato", d)
            if (d.children) {
                d._children = d.children;
                d.children = null;
            } else {
                //se ho nodi nascosti
                if (d._children) {
                    d.children = d._children;
                    //eseguo aggiungiChildren sui ciascuno dei children
                    //per averli già blu da espandere quando li mostro
                    d.children.forEach(aggiungiChildren)
                    d.children.forEach(collapse)
                    d._children = null;
                } else {
                    aggiungiChildren(d)
                }
            }
            console.log("dopo il click ho ottenuto ", d)
            var fraseCreata = calcolaFrase(d)
            console.log("calcolafrase", fraseCreata)
            Session.set("fraseCreata", fraseCreata);
            update(d);
            centerNode(d);
        }
    })
})

