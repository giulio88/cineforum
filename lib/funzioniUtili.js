logMatr = function (nome, m) {
    console.log(nome)
    for (i = 0; i < m.length; i++) {
        console.log(m[i])
    }
}


calcolaMedia = function (_id) {
    //mi restituisce una lista di Object
    var votiObject = VotiArchivio.find({
        destinatario: _id
    }).fetch()

    //prendo l'array dei votanti
    var votanti = _.pluck(votiObject, "mittente")

    //controllo se l'utente ha votato
    if (votanti.indexOf(Meteor.user().username) > -1) {

        //prendo l'array dei voti
        var voti = _.pluck(votiObject, "voto")

        //calcolo media
        media = _.reduce(voti, function (memo, num) {
            return memo + num;
        }, 0) / (voti.length === 0 ? 1 : voti.length);

        //arrotondo a un paio di decimali
        return Math.round(media * 100) / 100
    } else {
        return false
    }
}


/* SHULTZE METHOD
# Input: d[i,j], the number of voters who prefer candidate i to candidate j.
# Output: p[i,j], the strength of the strongest path from candidate i to candidate j.
 
for i from 1 to C
   for j from 1 to C
      if (i ≠ j) then
         if (d[i,j] > d[j,i]) then
            p[i,j] := d[i,j]
         else
            p[i,j] := 0
 
for i from 1 to C
   for j from 1 to C
      if (i ≠ j) then
         for k from 1 to C
            if (i ≠ k and j ≠ k) then
               p[j,k] := max ( p[j,k], min ( p[j,i], p[i,k] ) )
*/
calcolaStrongestPaths = function (d) {
    var C = d.length
    var p = new Array(C)
    for (var i = 0; i < C; i++) {
        p[i] = new Array(C);
    }
    for (var i = 0; i < C; i++) {
        for (var j = 0; j < C; j++) {
            p[i][j] = 0;
        }
    }
    console.log("C", C)
    logMatr("p", p)
    logMatr("d", d)
    for (i = 0; i < C; i++) {
        for (j = 0; j < C; j++) {
            if (i != j) {
                if (d[i][j] > d[j][i]) {
                    p[i][j] = d[i][j]
                    console.log("d[" + i + "][" + j + "] > d[" + j + "][" + i + "]", d[i][j], d[j][i], "lo copio in p")
                } else {
                    console.log("d[" + i + "][" + j + "] <= d[" + j + "][" + i + "]", d[i][j], d[j][i], "lo pongo 0")
                    p[i][j] = 0
                }
            }
        }
    }

    logMatr("p interm", p)

    for (i = 0; i < C; i++) {
        console.log("i=", i)
        for (j = 0; j < C; j++) {
            if (i != j) {
                for (k = 0; k < C; k++) {
                    if ((i != k) && (j != k)) {
                        p[j][k] = Math.max(p[j][k], Math.min(p[j][i], p[i][k]))
                        console.log("p[" + j + "][" + k + "]=", p[j][k])
                    }
                }
            }
        }
    }

    logMatr("p fin", p)

    return p
}


//ipotizzo matrici quadrate!
sommaMatrici = function (arrayMatrici) {
    //console.log("arrayMatrici", arrayMatrici)
    var somma = arrayMatrici[0];
    /*    console.log("somma prima")
        for (i = 0; i < somma.length; i++) {
            console.log(somma[i])
        }*/

    for (conto = 1; conto < arrayMatrici.length; conto++) {
        for (i = 0; i < arrayMatrici[conto].length; i++) {
            //console.log("arrayMatrici[conto][i]", arrayMatrici[conto][i], conto, i)
            for (j = 0; j < arrayMatrici[conto].length; j++) {
                somma[i][j] += arrayMatrici[conto][i][j];
            }
        }
    }

    /*    console.log("sommafinale")
        for (i = 0; i < somma.length; i++) {
            console.log(somma[i])
        }*/
    return somma;
}

vettoreToMatrice = function (vettoreVoto) {
    //trasformo il vettore in una matrice
    //perche' e' piu' comodo per sommarle poi
    //console.log("vettore originale", vettoreVoto, vettoreVoto.length)
    matriceVoto = [];
    for (i = 0; i < vettoreVoto.length; i++) {
        matriceVoto[i] = [] //array bidimensionale
        for (j = 0; j < vettoreVoto.length; j++) {
            if (vettoreVoto[i] < vettoreVoto[j]) {
                //se minore vuol dire che e' sopra in classifica
                //quindi vince, quindi metto 1 in matrice
                matriceVoto[i][j] = 1;
            } else {
                matriceVoto[i][j] = 0;
            }
        }
    }
    /*    console.log("matrice generata")
        for (i = 0; i < matriceVoto.length; i++) {
            console.log(matriceVoto[i])
        }*/
    return matriceVoto;
}


//per un destinatario calcolo la sua matrice somma dei voti
calcolaMatriceSomma = function (dest) {

    var matriceSomma = []

    var vettori = Voti.find({
        destinatario: dest,
        partecipo: true,
        vettore: {
            $exists: true
        }
    }).map(function (voto) {
        return voto.vettore
    })

    //converto ciascun voto in matrice
    var matrici = vettori.map(function (voto) {
        return vettoreToMatrice(voto)
    })

    /*    for (conto = 0; conto < matrici.length; conto++) {
            console.log("matrice n°", conto, " di ", dest)
            for (i = 0; i < matrici[conto].length; i++) {
                console.log(matrici[conto][i])
            }
        }*/

    //sommo le matrici
    matriceSomma = sommaMatrici(matrici)

    console.log("matriceSomma di ", dest)
    logMatr("", matriceSomma)

    return matriceSomma
}


calcolaClassifica = function (strpath, dest) {

    //lego l'indice al film
    film = []
    Proposte.findOne({
        utente: dest
    }).films.forEach(function (movie, key) {
        film[key] = movie
    })

    //memorizzo in vittorie tutte le volte che qualcuno vince
    var C = strpath.length;
    var vittorie = new Array(C)
    for (var i = 0; i < C; i++) {
        vittorie[i] = new Array();
    }

    for (i = 0; i < C; i++) {
        for (j = 0; j < C; j++) {
            if (i != j) {
                if (strpath[i][j] > strpath[j][i]) {
                    console.log("film ", i, " vince contro film ", j)
                    vittorie[i].push(j);
                }
            }
        }
    }

    console.log("vittorie", vittorie)

    var numVittorie = vittorie.map(function (vett) {
        return vett.length
    })

    console.log(numVittorie)

    var classifica = numVittorie.map(function (numero, key) {
        return {
            film: film[key],
            punteggio: numero
        }
    })

    classifica.sort(function (a, b) {
        return b.punteggio - a.punteggio;
    });

    console.log("classifica", classifica)

    return classifica;
}