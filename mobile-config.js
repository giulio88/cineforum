App.info({
    name: '#OCCUPYTHECOUCH',
    description: 'Cineforum',
    version: '1.0'
});

App.icons({
    'android_ldpi': 'resources/icons/icon-ldpi.png',
    'android_mdpi': 'resources/icons/icon-mdpi.png',
    'android_hdpi': 'resources/icons/icon-hdpi.png',
    'android_xhdpi': 'resources/icons/icon-xhdpi.png'
});

App.accessRule("*");